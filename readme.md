## Compilation

`gcc autoebs.c -o autoebs`

## Usage

`./autoebs <-excessiveblocksize bytes> [-autoebstart height] [-printaggregates]`
### Example 1

`./autoebs -excessiveblocksize 1000000 -autoebstart 0 -printaggregates <blocksizes.csv`

will print aggregates for each blockchain day (144 blocks)

### Example 2

`./autoebs -excessiveblocksize 1000000 -autoebstart 0 <blocksizes.csv`

will print values for each block

