#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define BLOCKCHAIN_DAY 144

typedef __uint128_t uint128_t;

typedef struct {
	uint32_t growthFactorNumerator;
	uint32_t growthFactorDenominator;
	uint8_t headroomFactor;
	uint64_t activationHeight;
} configAEBS_t;

uint64_t calculateAutoExcessiveBlocksize (uint64_t previousBlockSize, uint64_t previousExcessiveBlocksize, configAEBS_t *configAutoExcessiveBlocksize) {
	if (previousBlockSize * configAutoExcessiveBlocksize->headroomFactor > previousExcessiveBlocksize) {
		uint64_t excessiveBlockSize;
		excessiveBlockSize = (uint128_t) previousExcessiveBlocksize * configAutoExcessiveBlocksize->growthFactorNumerator / configAutoExcessiveBlocksize->growthFactorDenominator;
		// Required for really small configAutoExcessiveBlocksize where
		// calculated increase would be less than 1 byte and get truncated
		if (excessiveBlockSize == previousExcessiveBlocksize)
			excessiveBlockSize++;
		return excessiveBlockSize;
	}
	else 
		return previousExcessiveBlocksize;
}

int getWindowData (uint64_t *windowBlockHeights, uint64_t *windowBlockSizes, size_t windowLength) {
	for (size_t i=0; i<windowLength; i++) {
		if (scanf("%lu,%lu", &windowBlockHeights[i], &windowBlockSizes[i]) != 2)
			return 1;
	}
	return 0;
}

void calculateWindowExcessiveBlocksizes (
	uint64_t *windowBlockHeights,
	uint64_t *windowBlockSizes,
	uint64_t *windowExcessiveBlockSizes,
	size_t windowLength,
	uint64_t previousBlockSize,
	uint64_t previousExcessiveBlocksize,
	uint64_t configExcessiveBlocksize,
	configAEBS_t* configAutoExcessiveBlocksize)
{
	if (configAutoExcessiveBlocksize == NULL || configAutoExcessiveBlocksize->activationHeight > windowBlockHeights[0])
		windowExcessiveBlockSizes[0] = configExcessiveBlocksize;
	else
		windowExcessiveBlockSizes[0] = calculateAutoExcessiveBlocksize(previousBlockSize, previousExcessiveBlocksize, configAutoExcessiveBlocksize);
	for (int i=1; i<windowLength; i++) {
		if (configAutoExcessiveBlocksize == NULL || configAutoExcessiveBlocksize->activationHeight > windowBlockHeights[i])
			windowExcessiveBlockSizes[i] = configExcessiveBlocksize;
		else
			windowExcessiveBlockSizes[i] = calculateAutoExcessiveBlocksize(windowBlockSizes[i-1], windowExcessiveBlockSizes[i-1], configAutoExcessiveBlocksize);
	}
}

void printWindow (uint64_t *windowBlockHeights, uint64_t *windowBlockSizes, uint64_t *windowExcessiveBlockSizes, size_t windowLength) {
	for (size_t i=0; i<windowLength; i++)
		printf("%lu,%lu,%lu\n", windowBlockHeights[i], windowBlockSizes[i], windowExcessiveBlockSizes[i]);
}

int cmpUint64 (const void * a, const void * b) {
	if (*(uint64_t*)a = *(uint64_t*)b)
		return 0;
	else
		return *(uint64_t*)a < *(uint64_t*)b ? -1 : 1;
}

void printWindowAggregates (uint64_t *windowBlockHeights, uint64_t *windowBlockSizes, uint64_t *windowExcessiveBlockSizes, size_t windowLength) {
	uint64_t minBlockSize;
	uint64_t *sortedBlockSizes = malloc(windowLength * sizeof(uint64_t));
	uint64_t medianBlockSize;
	uint64_t maxBlockSize;
	minBlockSize = windowBlockSizes[0];
	maxBlockSize = windowBlockSizes[0];
	for (int i=0; i<windowLength; i++) {
		minBlockSize = windowBlockSizes[i] < minBlockSize ? windowBlockSizes[i] : minBlockSize;
		maxBlockSize = windowBlockSizes[i] > maxBlockSize ? windowBlockSizes[i] : maxBlockSize;
	}
	memcpy(sortedBlockSizes, windowBlockSizes, windowLength * sizeof(uint64_t));
	qsort(sortedBlockSizes, windowLength, sizeof(uint64_t), cmpUint64);
	medianBlockSize = sortedBlockSizes[windowLength/2];
	printf("%lu,%lu,%lu,%lu,%lu,%lu,%lu\n", windowBlockHeights[0], windowBlockHeights[windowLength-1], minBlockSize, medianBlockSize, maxBlockSize, windowExcessiveBlockSizes[0], windowExcessiveBlockSizes[windowLength-1]);

	free(sortedBlockSizes);
}

// Usage: ./autoebs <-excessiveblocksize 1000000> [-autoebstart 79400] [-printaggregates]
int main (int argc, char *argv[])
{
	// Process arguments
	uint64_t configExcessiveBlocksize;
	configAEBS_t* configAutoExcessiveBlocksize=NULL;
	int configPrintAggregates=0;
	if (argc < 3)
		return 1;
	if (!strcmp(argv[1], "-excessiveblocksize")) {
		if (sscanf(argv[2], "%lu", &configExcessiveBlocksize) < 1)
			return 1;
		if (configExcessiveBlocksize < 285)
			return 1;
	}
	else
		return 1;
	if (argc > 3 && ! strcmp(argv[3], "-autoebstart")) {
		uint64_t configAEBActivationHeight;
		if (sscanf(argv[4], "%lu", &configAEBActivationHeight) < 1)
			return 1;
		configAutoExcessiveBlocksize = malloc(sizeof(configAEBS_t));
		configAutoExcessiveBlocksize->headroomFactor = 8;
		configAutoExcessiveBlocksize->growthFactorNumerator = 4294967295u;
		configAutoExcessiveBlocksize->growthFactorDenominator = 4294910693u;
		configAutoExcessiveBlocksize->activationHeight = configAEBActivationHeight;
	}
	if (argc > 3 && ! strcmp(argv[argc-1], "-printaggregates")) {
		configPrintAggregates = 1;
	}

	// Initialize cache
	size_t windowLength = BLOCKCHAIN_DAY;
	uint64_t *windowBlockHeights = malloc(windowLength * sizeof(uint64_t));
	uint64_t *windowBlockSizes = malloc(windowLength * sizeof(uint64_t));
	uint64_t *windowExcessiveBlockSizes = malloc(windowLength * sizeof(uint64_t));

	// Print config used
	printf("; -excessiveblocksize %lu", configExcessiveBlocksize);
	if (configAutoExcessiveBlocksize != NULL)
		printf(" -autoebstart %lu", configAutoExcessiveBlocksize->activationHeight);
	printf("\n");

	// Print row headers
	if (configPrintAggregates)
		printf("openHeight,closeHeight,minBlockSize,medianBlockSize,maxBlocksize,openExcessiveBlocksize,closeExcessiveBlocksize\n");
	else
		printf("blockHeight,blockSize,excessiveBlocksize\n");

	// Calculate and print
	uint64_t previousBlockSize = 0;
	uint64_t previousExcessiveBlockSize = configExcessiveBlocksize;
	while (!getWindowData(windowBlockHeights, windowBlockSizes, windowLength)) {
		calculateWindowExcessiveBlocksizes(windowBlockHeights, windowBlockSizes, windowExcessiveBlockSizes, windowLength, previousBlockSize, previousExcessiveBlockSize, configExcessiveBlocksize, configAutoExcessiveBlocksize);
		if (configPrintAggregates)
			printWindowAggregates(windowBlockHeights, windowBlockSizes, windowExcessiveBlockSizes, windowLength);
		else
			printWindow(windowBlockHeights, windowBlockSizes, windowExcessiveBlockSizes, windowLength);
		previousBlockSize = windowBlockSizes[windowLength-1];
		previousExcessiveBlockSize = windowExcessiveBlockSizes[windowLength-1];
	}

	return 0;
}
